
public class TesteSaca {
	public static void main(String[] args) {
		Conta conta = new ContaCorrente(123, 456);
		
		conta.deposita(30);
		try {
			conta.saca(200);
		} catch(SaldoInsuficienteException err) {
			System.out.println("Não tem saldo suficiente");
			System.out.println(err.getMessage());
		}
		
		
		System.out.println("Saldo Atual: "+conta.getSaldo());
	}
}
