package br.com.bytebank.banco.test.util;

import java.util.ArrayList;
import java.util.List;

public class TesteOutrosWrapper {

	public static void main(String[] args) {
		
		Integer idadeRef = Integer.valueOf(8);
		System.out.println(idadeRef.intValue());
		
		Double dRef = Double.valueOf(5.7);
		System.out.println(dRef.doubleValue());
		
		Boolean bRef = Boolean.FALSE;
		System.out.println(bRef.booleanValue());
		
		Number numero = Integer.valueOf(2);
		
		List<Number> lista = new ArrayList<>();
		
		lista.add(41233.23423);
		lista.add(4342);
		lista.add(432.7f);
		lista.add(42.5);
		lista.add(14);
		
		System.out.println(lista);
		
	}

}
